/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form;

import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.component.*;

public class AngularUiFormGenerator extends TypeScriptUiFormGenerator {

    @Override
    protected <T extends TypeScriptFormComponentComponent> Class<T> getFormViewComponentClass() {
        return (Class<T>) AngularFormComponentComponent.class;
    }

    @Override
    protected <T extends TypeScriptFormComponentPrototypeComponent> Class<T> getFormViewComponentPrototypeClass() {
        return (Class<T>) AngularFormComponentPrototypeComponent.class;
    }

    protected <T extends AngularUiFormHtmlTemplateComponent> Class<T> getFormViewHtmlTemplateComponentClass() {
        return (Class<T>) AngularUiFormHtmlTemplateComponent.class;
    }

    protected <T extends AngularFormPageComponentComponent> Class<T> getFormPageComponentClass() {
        return (Class<T>) AngularFormPageComponentComponent.class;
    }

    protected <T extends AngularFormPageComponentPrototypeComponent> Class<T> getFormPageComponentPrototypeClass() {
        return (Class<T>) AngularFormPageComponentPrototypeComponent.class;
    }

    protected <T extends AngularUiFormPageHtmlTemplateComponent> Class<T> getFormPageHtmlTemplateComponentClass() {
        return (Class<T>) AngularUiFormPageHtmlTemplateComponent.class;
    }


    @Override
    protected void process() {
        super.process();

        addComponent(getFormViewHtmlTemplateComponentClass());

        addComponent(getFormPageComponentClass());
        addComponent(getFormPageComponentPrototypeClass());
        addComponent(getFormPageHtmlTemplateComponentClass());
    }


}
