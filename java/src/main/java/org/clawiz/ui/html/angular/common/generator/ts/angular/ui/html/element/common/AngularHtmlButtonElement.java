/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.html.element.common;

import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlButtonElement;

public class AngularHtmlButtonElement extends HtmlButtonElement {


    private String  _buttonTextExpression;
    private boolean _buttonTextExpressionPrepared = false;

    @Override
    public String getButtonCaption() {
        if ( _buttonTextExpressionPrepared ) {
            return _buttonTextExpression;
        }

        if ( super.getButtonCaption() != null ) {
            if ( super.getButtonCaption().charAt(0) == '"') {
                _buttonTextExpression = StringUtils.substring(super.getButtonCaption(), 1, super.getButtonCaption().length()-1);
            } else {
                _buttonTextExpression = "{{" + super.getButtonCaption() + "}}";
            }
            _buttonTextExpressionPrepared = true;
            return _buttonTextExpression;
        }

        String[] tokens = StringUtils.splitByLastTokenOccurrence(getLayoutComponent().getClass().getName(), "\\.");
        int i = tokens[1].indexOf("Button");

        if ( i > 0 ) {
            _buttonTextExpression = "{{" + StringUtils.toLowerFirstChar(tokens[1].substring(0, i)) + "ButtonText}}";
        }

        _buttonTextExpressionPrepared = true;
        return _buttonTextExpression;
    }

    @Override
    protected String getOnClickAttributeName() {
        return "(click)";
    }

    @Override
    public void addVisible() {
        if ( getVisibleExpression() != null ) {
            addAttribute("*ngIf", getVisibleExpression());
        }
    }

    @Override
    protected String getDisabledAttributeName() {
        return "[disabled]";
    }

    @Override
    public void process() {
        super.process();

        if ( getAttribute("id") != null ) {
            setAttribute("#" + getAttribute("id"));
        }

    }
}
