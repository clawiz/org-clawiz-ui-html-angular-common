/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.component;

import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptFieldElement;
import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.button.AbstractButton;
import org.clawiz.ui.common.metadata.data.layout.container.AbstractContainer;
import org.clawiz.ui.common.metadata.data.view.form.field.AbstractFormField;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.TypeScriptFormComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.component.element.UiFormComponentPrepareMethodElement;
import org.clawiz.ui.html.angular.common.generator.ts.angular.AngularClientGeneratorUtils;
import org.clawiz.ui.html.angular.common.generator.ts.angular.element.AngularViewChildDecoratorElement;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.component.element.AngularUiFormComponentPrepareMethodElement;

public class AngularFormComponentPrototypeComponent extends TypeScriptFormComponentPrototypeComponent {

    protected void addImports() {
        addImport("ElementRef", "@angular/core");
        addImport("ViewChild", "@angular/core");
    }

    protected void addFieldReferences() {
        for (AbstractFormField formField : getForm().getFields()) {
            TypeScriptFieldElement field = addField(getGenerator().getNodeVariableName(formField) + "Element", "ElementRef");
            field.addElement(AngularViewChildDecoratorElement.class)
             .setSelector("'" + getGenerator().getNodeId(formField) + "'");
        }

        for( Grid grid : getGenerator().getContext().getGrids()) {
            TypeScriptFieldElement field = addField(getGenerator().getNodeVariableName(grid) + "Component", toTypeScriptType(grid));
            field.addElement(AngularViewChildDecoratorElement.class)
                    .setSelector("'" + getGenerator().getNodeId(grid) + "'");
        }

        for (AbstractButton button : getForm().getLayoutComponents().getComponentsByClass(AbstractButton.class)) {
            TypeScriptFieldElement field = addField(getGenerator().getNodeVariableName(button) + "Button", "ElementRef");
            field.addElement(AngularViewChildDecoratorElement.class)
                    .setSelector("'" + getGenerator().getNodeId(button) + "'");
        }

    }


    @Override
    public void prepare() {
        super.prepare();
        addElementClassReplacer(UiFormComponentPrepareMethodElement.class, AngularUiFormComponentPrepareMethodElement.class);
    }

    @Override
    public void process() {
        AngularClientGeneratorUtils.addUiConstructor(this);

        addImports();

        addFieldReferences();

        super.process();
    }

}
