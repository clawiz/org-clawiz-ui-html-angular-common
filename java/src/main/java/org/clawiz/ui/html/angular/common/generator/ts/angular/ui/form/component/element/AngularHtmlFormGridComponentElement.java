/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.component.element;

import org.clawiz.ui.common.metadata.data.layout.AbstractLayoutComponent;
import org.clawiz.ui.common.metadata.data.layout.component.GridLayoutComponent;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.grid.component.AngularGridComponentComponent;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.common.HtmlDivElement;

public class AngularHtmlFormGridComponentElement extends HtmlDivElement {

    Grid grid;

    public Grid getGrid() {
        return grid;
    }

    @Override
    public void setLayoutComponent(AbstractLayoutComponent layoutComponent) {
        super.setLayoutComponent(layoutComponent);
        grid = ((GridLayoutComponent) layoutComponent).getGrid();
    }

    AbstractHtmlBodyElement gridContainer;
    protected void addGridContainer() {
        gridContainer = addElement(AbstractHtmlBodyElement.class);
        gridContainer.setTagName(AngularGridComponentComponent.getSelectorName(grid));
        gridContainer.setSelfCloseEnabled(false);
    }

    public AbstractHtmlBodyElement getGridContainer() {
        return gridContainer;
    }

    @Override
    public FormGenerator getGenerator() {
        return (FormGenerator) super.getGenerator();
    }

    @Override
    public void process() {
        super.process();

        addGridContainer();

        getGridContainer().setAttribute("#" + getGenerator().getNodeId(((GridLayoutComponent) getLayoutComponent()).getGrid()));
    }
}
