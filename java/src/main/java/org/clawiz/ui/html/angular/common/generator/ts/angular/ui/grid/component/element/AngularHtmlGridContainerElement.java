/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.grid.component.element;

import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.table.HtmlTableBodyElement;
import org.clawiz.ui.html.common.generator.html.element.table.HtmlTableDataElement;
import org.clawiz.ui.html.common.generator.html.element.table.HtmlTableElement;
import org.clawiz.ui.html.common.generator.html.element.table.HtmlTableRowElement;
import org.clawiz.ui.html.common.generator.html.view.grid.element.HtmlGridContainerElement;

public class AngularHtmlGridContainerElement extends HtmlGridContainerElement {

    @Override
    protected void setTableBodyRowOnclick(HtmlTableRowElement rowElement) {
        rowElement.setAttribute("(click)", "onClickRow(i, $event)");
        rowElement.setAttribute("(dblclick)", "onDoubleClickRow(i, $event)");
    }

    @Override
    protected HtmlTableElement addTable(AbstractHtmlBodyElement content) {
        HtmlTableElement table = super.addTable(content);

        table.addAttribute("clawizMouseWheel");
        table.addAttribute("(mouseWheelUp)", "onMouseWheelUp($event)");
        table.addAttribute("(mouseWheelDown)", "onMouseWheelDown($event)");

        return table;
    }

    protected HtmlTableRowElement addTableBodyRow(HtmlTableBodyElement body) {
        return super.addTableBodyRow(body)
                .withAttribute("*ngFor", "let row of rowsDisplayBuffer; let i = index")
                .withAttribute("[class.clawiz-row-selected]", "i == pageSelectedRowIndex");
    }

    protected HtmlTableDataElement addDataColumn(HtmlTableRowElement tr, AbstractGridColumn column) {
        String pipe = null;

        if ( column.getValueType() instanceof ValueTypeNumber) {
            if ( column.getPrecision() != null  || column.getScale() != null ) {
                pipe = "number :'1" + (column.getScale() != null ? "." + column.getScale() : "") + "'";
            }
        } else if ( column.getValueType() instanceof ValueTypeDate) {
            pipe = "date : 'dd.MM.yyyy'";
        } else if ( column.getValueType() instanceof ValueTypeDateTime) {
            pipe = "date : 'dd.MM.yyyy HH:mm:ss'";
        }

        String text = "{{row." + column.getJavaVariableName()
                + (
                (
                        column.getValueType() instanceof ValueTypeEnumeration
                                || column.getValueType() instanceof ValueTypeObject
                ) ? "ToString" : "" )
                + ( pipe != null ? " | " + pipe : "" )
                + "}}";
        return super.addDataColumn(tr, column)
                .withText(text);
    }


    @SuppressWarnings("Duplicates")
    @Override
    public void process() {
        AbstractHtmlBodyElement ngxLoading = addElement(AbstractHtmlBodyElement.class);
        ngxLoading.setTagName("ngx-loading");
        ngxLoading.setSelfCloseEnabled(false);
        ngxLoading.addAttribute("[show]", "locked");

        super.process();



    }
}
