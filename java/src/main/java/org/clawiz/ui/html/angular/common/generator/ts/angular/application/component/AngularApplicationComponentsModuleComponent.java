/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.application.component;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.ui.common.metadata.data.view.form.Form;
import org.clawiz.ui.common.metadata.data.view.grid.Grid;
import org.clawiz.ui.common.generator.common.view.form.FormGeneratorContext;
import org.clawiz.ui.common.generator.common.view.grid.GridGeneratorContext;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.AbstractAngularApplicationTypeScriptClassComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.element.AngularNgModuleDecoratorElement;


public class AngularApplicationComponentsModuleComponent extends AbstractAngularApplicationTypeScriptClassComponent {

    AngularNgModuleDecoratorElement   ngModule;
    AngularApplicationRoutesComponent routes;

    String routesPrefix = " ";

    public AngularNgModuleDecoratorElement getNgModule() {
        return ngModule;
    }

    private void addDeclaredComponent(String componentPackageName, String componentName, String routePath, String routePath2) {
        String importString = getGenerator().getApplicationDestinationPath()
                + GeneratorUtils.getPackageRelativePath(getGenerator().getPackageName(), componentPackageName)
                + "/"
                + nameToDottedFileName(componentName);
        addImport(componentName, importString);

        ngModule.addDeclaration(componentName);
        ngModule.addExport(componentName);

        if ( routePath != null ) {
            routes.addImport(componentName, importString);
            routes.addText("      " + routesPrefix + "{ path: '" + routePath + "', component: " + componentName + "}");
            routesPrefix = ",";
        }
        if ( routePath2 != null ) {
            routes.addText("      " + routesPrefix + "{ path: '" + routePath2 + "', component: " + componentName + "}");
            routesPrefix = ",";
        }

    }

    protected void prepareRoutesComponent() {

        routes = getGenerator().getComponentByClass(AngularApplicationRoutesComponent.class);
        routes.setDestinationPath(getGenerator().getApplicationDestinationPath() + "/ui/component");
        routes.setName(getApplicationJavaClassName() + "Routes");
        routes.setFileName(nameToDottedFileName(routes.getName()) + ".ts");

        routes.addImport("ClawizBuilderReadyGuard", null, getGeneratorContext().getAngularCoreImportPath(), true);
        routes.addImport("UserLoggedCanActivateGuard", null, getGeneratorContext().getAngularCoreImportPath(), true);
        routes.addImport("DashboardPageComponent", null, getGenerator().getDestinationPath() + "/pages/dashboard/dashboard.page.component", true);

        routes.addText("export const " + getApplicationJavaClassName().toUpperCase() +  "_ROUTES =\n" +
                "  {\n" +
                "    path: '', component: DashboardPageComponent,\n" +
                "    canActivate: [\n" +
                "      ClawizBuilderReadyGuard.CLAWIZ_BUILDER_READY_GUARD, UserLoggedCanActivateGuard.USER_LOGGED_CAN_ACTIVATE_GUARD\n" +
                "    ],\n" +
                "\n" +
                "    children: [\n");
    }

    @Override
    public void process() {
        super.process();
        setDestinationPath(getGenerator().getApplicationDestinationPath() + "/ui/component");
        setName(getApplicationJavaClassName() + "ComponentsModule");

        prepareRoutesComponent();

        ngModule = addElement(AngularNgModuleDecoratorElement.class);

        addImport("BrowserModule", "@angular/platform-browser");
        addImport("FormsModule", "@angular/forms");
        addImport("NgModule", "@angular/core");


        ngModule.addImport("BrowserModule");
        ngModule.addImport("FormsModule");


        for ( FormGeneratorContext formGeneratorContext : getApplicationContext().getFormContexts() ) {
            Form form = formGeneratorContext.getForm();
            addDeclaredComponent( form.getPackageName() + "." + form.getJavaClassName().toLowerCase()
                    , form.getJavaClassName() + "Component"
                    , null
                    , null);
            addDeclaredComponent( form.getPackageName() + "." + form.getJavaClassName().toLowerCase()
                    ,form.getJavaClassName() + "PageComponent"
                   , StringUtils.toLowerFirstChar(form.getJavaClassName()) + "/:action"
                   , StringUtils.toLowerFirstChar(form.getJavaClassName()) + "/:action/:parameters");
        }

        for ( GridGeneratorContext gridGeneratorContext : getApplicationContext().getGridContexts() ) {
            Grid grid = gridGeneratorContext.getGrid();
            addDeclaredComponent( grid.getPackageName() + "." + grid.getJavaClassName().toLowerCase()
                    ,grid.getJavaClassName() + "Component"
                    ,null
                    ,null);
            addDeclaredComponent( grid.getPackageName() + "." + grid.getJavaClassName().toLowerCase()
                    , grid.getJavaClassName() + "PageComponent"
                    , StringUtils.toLowerFirstChar(grid.getJavaClassName())
                    , null);
        }

        routes.addText("    ]\n" +
                "  };");

    }
}
