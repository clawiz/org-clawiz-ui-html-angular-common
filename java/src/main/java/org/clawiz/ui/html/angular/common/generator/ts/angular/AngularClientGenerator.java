/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular;

import org.clawiz.ui.common.generator.AbstractUiGeneratorContext;
import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGenerator;
import org.clawiz.ui.common.generator.client.ts.application.ui.ApplicationAbstractUiComponentComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.ApplicationAbstractUiComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.router.ApplicationRouterServiceComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.router.ApplicationRouterServicePrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormComponentComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.form.ApplicationAbstractFormComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.grid.ApplicationAbstractGridComponentComponent;
import org.clawiz.ui.common.generator.client.ts.application.ui.grid.ApplicationAbstractGridComponentPrototypeComponent;
import org.clawiz.ui.common.generator.client.ts.ui.form.TypeScriptUiFormGenerator;
import org.clawiz.ui.common.generator.client.ts.ui.grid.TypeScriptUiGridGenerator;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.AngularApplicationAbstractUiComponentComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.AngularApplicationAbstractUiComponentPrototypeComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.AngularApplicationComponentsModuleComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.AngularApplicationRoutesComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.ui.form.AngularApplicationAbstractFormComponentComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.ui.form.AngularApplicationAbstractFormComponentPrototypeComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.ui.grid.AngularApplicationAbstractGridComponentComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.ui.grid.AngularApplicationAbstractGridComponentPrototypeComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.ui.router.AngularApplicationRouterServiceComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.application.component.ui.router.AngularApplicationRouterServicePrototypeComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.AngularUiFormGenerator;
import org.clawiz.ui.html.angular.common.generator.ts.angular.ui.grid.AngularUiGridGenerator;

public class AngularClientGenerator extends TypeScriptClientCommonGenerator {

    public <T extends AbstractApplicationGeneratorContext> Class<T> getApplicationGenerateContextClass() {
        return (Class<T>) AngularClientGeneratorContext.class;
    }

    private AngularClientGeneratorContext angularContext;

    @Override
    public void setContext(AbstractUiGeneratorContext context) {
        super.setContext(context);
        angularContext = (AngularClientGeneratorContext) context;
    }


    @Override
    public AngularClientGeneratorContext getContext() {
        return angularContext;
    }

    @Override
    protected void addServiceComponents() {
    }

    @Override
    protected void addStorageComponents() {
    }

    @Override
    public String getBuilderClassNamePrefix() {
        return "Angular";
    }

    @Override
    protected void addDataSourceGenerators() {
    }

    @Override
    public <T extends TypeScriptUiFormGenerator> Class<T> getFormGeneratorClass() {
        return (Class<T>) AngularUiFormGenerator.class;
    }

    public <T extends TypeScriptUiGridGenerator> Class<T> getGridGeneratorClass() {
        return (Class<T>) AngularUiGridGenerator.class;
    }

    @Override
    protected void prepareComponentsMap() {
        super.prepareComponentsMap();

        addComponentMap(ApplicationAbstractFormComponentComponent.class, AngularApplicationAbstractFormComponentComponent.class);
        addComponentMap(ApplicationAbstractFormComponentPrototypeComponent.class, AngularApplicationAbstractFormComponentPrototypeComponent.class);
        addComponentMap(ApplicationAbstractGridComponentComponent.class, AngularApplicationAbstractGridComponentComponent.class);
        addComponentMap(ApplicationAbstractGridComponentPrototypeComponent.class, AngularApplicationAbstractGridComponentPrototypeComponent.class);

        addComponentMap(ApplicationRouterServiceComponent.class, AngularApplicationRouterServiceComponent.class);
        addComponentMap(ApplicationRouterServicePrototypeComponent.class, AngularApplicationRouterServicePrototypeComponent.class);


        addComponentMap(ApplicationAbstractUiComponentComponent.class, AngularApplicationAbstractUiComponentComponent.class);
        addComponentMap(ApplicationAbstractUiComponentPrototypeComponent.class, AngularApplicationAbstractUiComponentPrototypeComponent.class);
    }

    @Override
    protected void process() {
        super.process();

        addComponent(AngularApplicationRoutesComponent.class);
        addComponent(AngularApplicationComponentsModuleComponent.class);

    }

    @Override
    protected void done() {
        super.done();

    }

}
