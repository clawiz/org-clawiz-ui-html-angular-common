/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular;

import org.clawiz.ui.common.generator.client.ts.TypeScriptClientCommonGeneratorContext;

public class AngularClientGeneratorContext extends TypeScriptClientCommonGeneratorContext {

    public String getApplicationDestinationPath() {
        return super.getDestinationPath() + "/" + getApplicationJavaClassName().toLowerCase() + "/angular";
    }

    private String clawizCoreAngularImportPath = "clawiz-core-angular";
    public String getAngularCoreImportPath() {
        return clawizCoreAngularImportPath;
    }

    public void setClawizCoreAngularImportPath(String clawizCoreAngularImportPath) {
        this.clawizCoreAngularImportPath = clawizCoreAngularImportPath;
    }

    @Override
    protected void prepareImportMaps() {
        super.prepareImportMaps();

        addImportMap(CLASS_NAME_CLAWIZ_BUILDER,               "AngularClawizBuilder",               getAngularCoreImportPath());
        addImportMap(CLASS_NAME_CLAWIZ_CONFIG,                "AngularClawizConfig",                getAngularCoreImportPath());
        addImportMap(CLASS_NAME_CLAWIZ_CONFIG_IMPLEMENTATION, "AngularClawizConfigImplementation",  getAngularCoreImportPath());
        addImportMap(CLASS_NAME_CLAWIZ,                       "AngularClawiz",                      getAngularCoreImportPath());
        addImportMap(CLASS_NAME_CLAWIZ_IMPLEMENTATION,        "AngularClawizImplementation",        getAngularCoreImportPath());

        addImportMap(CLASS_NAME_ABSTRACT_UI_COMPONENT,     "AbstractAngularUiComponent",         getAngularCoreImportPath());
        addImportMap(CLASS_NAME_ABSTRACT_FORM_COMPONENT,   "AbstractAngularFormComponent",          getAngularCoreImportPath());
        addImportMap(CLASS_NAME_ABSTRACT_GRID_COMPONENT,   "AbstractAngularGridComponent",          getAngularCoreImportPath());

        addImportMap(CLASS_NAME_ABSTRACT_ROUTER_SERVICE, "AbstractAngularRouterService",            getAngularCoreImportPath());

        addImportMap(CLASS_NAME_APPLICATION_STORAGE_SERVICE, getApplicationJavaClassName() + "StorageService", getClientCommonImportPath());




    }
}
