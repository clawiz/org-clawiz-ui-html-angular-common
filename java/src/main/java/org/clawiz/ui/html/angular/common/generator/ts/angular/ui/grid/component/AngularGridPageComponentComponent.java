/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.grid.component;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.ui.common.generator.client.ts.ui.grid.component.AbstractTypeScriptGridClassComponent;
import org.clawiz.ui.html.angular.common.generator.ts.angular.AngularClientGeneratorUtils;
import org.clawiz.ui.html.angular.common.generator.ts.angular.element.AngularComponentDecoratorElement;

public class AngularGridPageComponentComponent extends AbstractTypeScriptGridClassComponent {

    @Override
    public void process() {
        AngularClientGeneratorUtils.addUiConstructor(this);

        super.process();

        setName(getGrid().getJavaClassName() + "PageComponent");

        setOverwriteMode(AbstractComponent.OverwriteMode.SKIP);

        setExtendsOfPrototype();


        addImport("Component", "@angular/core");

        addElement(AngularComponentDecoratorElement.class)
                .withSelector(AngularGridComponentComponent.getSelectorName(getGrid()) + "-page")
                .withTemplateUrl(nameToDottedFileName(getGrid().getJavaClassName() + ".page.html"));

    }
}
