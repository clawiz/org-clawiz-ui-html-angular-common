package org.clawiz.ui.html.angular.common.install;

import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.install.client.ts.common.XMLScriptGenerateApplicationTypeScriptCommonInstaller;
import org.clawiz.ui.html.angular.common.generator.ts.angular.AngularClientGeneratorContext;

public class XMLScriptGenerateApplicationAngularInstaller extends XMLScriptGenerateApplicationTypeScriptCommonInstaller {

    public static String CLAWIZ_CORE_ANGULAR_IMPORT_PATH_ATTRIBUTE_NAME = "clawiz-core-angular-import-path";

    @Override
    protected void fillApplicationGeneratorContext(AbstractApplicationGeneratorContext applicationGeneratorContext) {
        super.fillApplicationGeneratorContext(applicationGeneratorContext);

        AngularClientGeneratorContext context = (AngularClientGeneratorContext) applicationGeneratorContext;

        String angularCoreImportPath = getAttribute(CLAWIZ_CORE_ANGULAR_IMPORT_PATH_ATTRIBUTE_NAME);
        if ( angularCoreImportPath != null ) {
            context.setClawizCoreAngularImportPath(angularCoreImportPath);
        }

    }
}
