/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.angular.common.generator.ts.angular.ui.form.component.element;

import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDate;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeDateTime;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.ui.html.common.generator.html.element.AbstractHtmlBodyElement;
import org.clawiz.ui.html.common.generator.html.element.form.HtmlInputElement;
import org.clawiz.ui.html.common.generator.html.element.form.HtmlSelectElement;
import org.clawiz.ui.html.common.generator.html.element.form.HtmlSelectOptionElement;
import org.clawiz.ui.html.common.generator.html.view.form.element.HtmlFormFieldElement;

public class AngularHtmlFormFieldElement extends HtmlFormFieldElement {



    @Override
    protected void addSelectOptions(HtmlSelectElement selectElement) {
        HtmlSelectOptionElement option = selectElement.addElement(HtmlSelectOptionElement.class);
        option.setAttribute("*ngFor", "let option of data." + getFormField().getJavaVariableName() + "SelectOptions");
        option.setAttribute("[value]", "option.value");
        option.setText("{{option.text}}");
    }

    @Override
    protected HtmlInputElement createInput(AbstractHtmlBodyElement container) {
        HtmlInputElement input = super.createInput(container);


        String modelName = "data." + getFormField().getJavaVariableName();
        if (  getFormField().getValueType() instanceof ValueTypeDate
                || getFormField().getValueType() instanceof ValueTypeDateTime
                || getFormField().getValueType() instanceof ValueTypeObject) {
            modelName = modelName + "ToString";
        }
        input.setAttribute("[(ngModel)]", modelName);
        input.setAttribute("#" + input.getAttribute("id"));

        return input;
    }

    @Override
    public void process() {
        super.process();
    }
}
